# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140905140204) do

  create_table "app_configs", force: :cascade do |t|
    t.string   "footer",             limit: 255
    t.string   "auth_types",         limit: 255
    t.string   "ldap_host_address",  limit: 255
    t.integer  "ldap_port",          limit: 4
    t.string   "ldap_base",          limit: 255
    t.string   "ldap_login",         limit: 255
    t.string   "ldap_first_name",    limit: 255
    t.string   "ldap_last_name",     limit: 255
    t.string   "ldap_email",         limit: 255
    t.boolean  "use_ldap",           limit: 1
    t.string   "mailer_address",     limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "calendar_feed_hash", limit: 255
    t.string   "admin_email",        limit: 255
  end

  create_table "calendars", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.datetime "start_date"
    t.datetime "end_date"
    t.boolean  "active",        limit: 1
    t.integer  "department_id", limit: 4
    t.boolean  "default",       limit: 1,   default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public",        limit: 1
  end

  create_table "categories", force: :cascade do |t|
    t.boolean  "active",        limit: 1,   default: true
    t.boolean  "built_in",      limit: 1,   default: false
    t.string   "name",          limit: 255
    t.integer  "department_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "billing_code",  limit: 255
  end

  create_table "data_entries", force: :cascade do |t|
    t.integer  "data_object_id", limit: 4
    t.text     "content",        limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "data_fields", force: :cascade do |t|
    t.integer  "data_type_id", limit: 4
    t.string   "name",         limit: 255
    t.string   "display_type", limit: 255
    t.string   "values",       limit: 255
    t.float    "upper_bound",  limit: 24
    t.float    "lower_bound",  limit: 24
    t.string   "exact_alert",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "active",       limit: 1,   default: true
  end

  create_table "data_objects", force: :cascade do |t|
    t.integer  "data_type_id", limit: 4
    t.string   "name",         limit: 255
    t.string   "description",  limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "data_objects_locations", id: false, force: :cascade do |t|
    t.integer "data_object_id", limit: 4
    t.integer "location_id",    limit: 4
  end

  create_table "data_types", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.text     "description",   limit: 65535
    t.integer  "department_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0
    t.integer  "attempts",   limit: 4,     default: 0
    t.text     "handler",    limit: 65535
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "department_configs", force: :cascade do |t|
    t.integer  "department_id",        limit: 4
    t.integer  "schedule_start",       limit: 4
    t.integer  "schedule_end",         limit: 4
    t.integer  "time_increment",       limit: 4
    t.integer  "grace_period",         limit: 4
    t.boolean  "auto_remind",          limit: 1,     default: true
    t.boolean  "auto_warn",            limit: 1,     default: true
    t.string   "mailer_address",       limit: 255
    t.boolean  "monthly",              limit: 1,     default: false
    t.boolean  "end_of_month",         limit: 1,     default: false
    t.integer  "day",                  limit: 4,     default: 6
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "weekend_shifts",       limit: 1
    t.boolean  "unscheduled_shifts",   limit: 1
    t.text     "printed_message",      limit: 65535
    t.text     "reminder_message",     limit: 65535
    t.text     "warning_message",      limit: 65535
    t.integer  "warning_weeks",        limit: 4
    t.integer  "description_min",      limit: 4
    t.integer  "reason_min",           limit: 4
    t.boolean  "can_take_passed_sub",  limit: 1,     default: true
    t.string   "stats_mailer_address", limit: 255
    t.boolean  "stale_shift",          limit: 1,     default: true
    t.integer  "payform_time_limit",   limit: 4
    t.integer  "admin_round_option",   limit: 4,     default: 15
    t.integer  "early_signin",         limit: 4,     default: 60
    t.integer  "task_leniency",        limit: 4,     default: 60
    t.string   "search_engine_name",   limit: 255,   default: "Google"
    t.string   "search_engine_url",    limit: 255,   default: "http://www.google.com/search?q="
    t.integer  "default_category_id",  limit: 4
  end

  create_table "departments", force: :cascade do |t|
    t.string   "name",                   limit: 255
    t.integer  "admin_permission_id",    limit: 4
    t.integer  "payforms_permission_id", limit: 4
    t.integer  "shifts_permission_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "departments_users", id: false, force: :cascade do |t|
    t.integer  "department_id", limit: 4
    t.integer  "user_id",       limit: 4
    t.boolean  "active",        limit: 1,                          default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "payrate",                 precision: 10, scale: 2
  end

  create_table "emails", force: :cascade do |t|
    t.string   "from",              limit: 255
    t.string   "to",                limit: 255
    t.integer  "last_send_attempt", limit: 4,     default: 0
    t.text     "mail",              limit: 65535
    t.datetime "created_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "loc_groups", force: :cascade do |t|
    t.string   "name",           limit: 255
    t.integer  "department_id",  limit: 4
    t.integer  "view_perm_id",   limit: 4
    t.integer  "signup_perm_id", limit: 4
    t.integer  "admin_perm_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public",         limit: 1,   default: true
    t.boolean  "active",         limit: 1,   default: true
  end

  create_table "loc_groups_notices", id: false, force: :cascade do |t|
    t.integer "loc_group_id", limit: 4
    t.integer "notice_id",    limit: 4
  end

  create_table "loc_groups_restrictions", id: false, force: :cascade do |t|
    t.integer "restriction_id", limit: 4
    t.integer "loc_group_id",   limit: 4
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.string   "short_name",   limit: 255
    t.text     "useful_links", limit: 65535
    t.integer  "max_staff",    limit: 4
    t.integer  "min_staff",    limit: 4
    t.integer  "priority",     limit: 4
    t.string   "report_email", limit: 255
    t.boolean  "active",       limit: 1
    t.integer  "loc_group_id", limit: 4
    t.integer  "template_id",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description",  limit: 255
    t.integer  "category_id",  limit: 4
  end

  create_table "locations_notices", id: false, force: :cascade do |t|
    t.integer "location_id", limit: 4
    t.integer "notice_id",   limit: 4
  end

  create_table "locations_requested_shifts", id: false, force: :cascade do |t|
    t.integer  "requested_shift_id", limit: 4
    t.integer  "location_id",        limit: 4
    t.boolean  "assigned",           limit: 1, default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "locations_restrictions", id: false, force: :cascade do |t|
    t.integer "restriction_id", limit: 4
    t.integer "location_id",    limit: 4
  end

  create_table "locations_shift_preferences", id: false, force: :cascade do |t|
    t.integer  "shift_preference_id", limit: 4
    t.integer  "location_id",         limit: 4
    t.string   "kind",                limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notices", force: :cascade do |t|
    t.boolean  "sticky",          limit: 1,     default: false
    t.boolean  "useful_link",     limit: 1,     default: false
    t.boolean  "announcement",    limit: 1,     default: false
    t.boolean  "indefinite",      limit: 1
    t.text     "content",         limit: 65535
    t.integer  "author_id",       limit: 4
    t.datetime "start"
    t.datetime "end"
    t.integer  "department_id",   limit: 4
    t.integer  "remover_id",      limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "url",             limit: 255
    t.string   "type",            limit: 255
    t.boolean  "department_wide", limit: 1
  end

  create_table "payform_item_sets", force: :cascade do |t|
    t.integer "category_id",    limit: 4
    t.date    "date"
    t.decimal "hours",                        precision: 10, scale: 2
    t.text    "description",    limit: 65535
    t.boolean "active",         limit: 1
    t.integer "approved_by_id", limit: 4
  end

  create_table "payform_items", force: :cascade do |t|
    t.integer  "category_id",         limit: 4
    t.integer  "user_id",             limit: 4
    t.integer  "payform_id",          limit: 4
    t.integer  "payform_item_set_id", limit: 4
    t.boolean  "active",              limit: 1,                              default: true
    t.decimal  "hours",                             precision: 10, scale: 2
    t.date     "date"
    t.text     "description",         limit: 65535
    t.text     "reason",              limit: 65535
    t.string   "source",              limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "source_url",          limit: 255
  end

  create_table "payform_sets", force: :cascade do |t|
    t.integer  "department_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payforms", force: :cascade do |t|
    t.date     "date"
    t.boolean  "monthly",        limit: 1,                          default: false
    t.boolean  "end_of_month",   limit: 1,                          default: false
    t.integer  "day",            limit: 4,                          default: 6
    t.datetime "submitted"
    t.datetime "approved"
    t.datetime "printed"
    t.integer  "approved_by_id", limit: 4
    t.integer  "department_id",  limit: 4
    t.integer  "user_id",        limit: 4
    t.integer  "payform_set_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.decimal  "payrate",                  precision: 10, scale: 2
    t.datetime "skipped"
  end

  create_table "permissions", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "permissions_roles", id: false, force: :cascade do |t|
    t.integer "role_id",       limit: 4
    t.integer "permission_id", limit: 4
  end

  create_table "punch_clock_sets", force: :cascade do |t|
    t.string   "description",   limit: 255
    t.integer  "department_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "punch_clocks", force: :cascade do |t|
    t.string   "description",        limit: 255
    t.integer  "user_id",            limit: 4
    t.integer  "department_id",      limit: 4
    t.integer  "runtime",            limit: 4
    t.datetime "last_touched"
    t.boolean  "paused",             limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "punch_clock_set_id", limit: 4
  end

  create_table "repeating_events", force: :cascade do |t|
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "start_time"
    t.datetime "end_time"
    t.integer  "calendar_id",         limit: 4
    t.string   "days_of_week",        limit: 255
    t.integer  "user_id",             limit: 4
    t.string   "loc_ids",             limit: 255
    t.boolean  "is_set_of_timeslots", limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "report_items", force: :cascade do |t|
    t.integer  "report_id",  limit: 4
    t.datetime "time"
    t.text     "content",    limit: 65535
    t.string   "ip_address", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reports", force: :cascade do |t|
    t.integer  "shift_id",   limit: 4
    t.datetime "arrived"
    t.datetime "departed"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "requested_shifts", force: :cascade do |t|
    t.datetime "preferred_start"
    t.datetime "preferred_end"
    t.datetime "acceptable_start"
    t.datetime "acceptable_end"
    t.integer  "day",              limit: 4
    t.integer  "template_id",      limit: 4
    t.integer  "user_id",          limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "assigned_start"
    t.datetime "assigned_end"
  end

  create_table "restrictions", force: :cascade do |t|
    t.datetime "starts"
    t.datetime "expires"
    t.integer  "max_subs",   limit: 4
    t.decimal  "max_hours",            precision: 10, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "restrictions_users", id: false, force: :cascade do |t|
    t.integer "restriction_id", limit: 4
    t.integer "user_id",        limit: 4
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.integer  "department_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_templates", id: false, force: :cascade do |t|
    t.integer  "role_id",     limit: 4
    t.integer  "template_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", id: false, force: :cascade do |t|
    t.integer "role_id", limit: 4
    t.integer "user_id", limit: 4
  end

  create_table "shift_preferences", force: :cascade do |t|
    t.integer  "max_total_hours",      limit: 4
    t.integer  "min_total_hours",      limit: 4
    t.integer  "max_continuous_hours", limit: 4
    t.integer  "min_continuous_hours", limit: 4
    t.integer  "max_number_of_shifts", limit: 4
    t.integer  "min_number_of_shifts", limit: 4
    t.integer  "max_hours_per_day",    limit: 4
    t.integer  "template_id",          limit: 4
    t.integer  "user_id",              limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "shifts", force: :cascade do |t|
    t.datetime "start"
    t.datetime "end"
    t.boolean  "active",              limit: 1
    t.integer  "calendar_id",         limit: 4
    t.integer  "repeating_event_id",  limit: 4
    t.integer  "user_id",             limit: 4
    t.integer  "location_id",         limit: 4
    t.integer  "department_id",       limit: 4
    t.boolean  "scheduled",           limit: 1,                         default: true
    t.boolean  "signed_in",           limit: 1,                         default: false
    t.boolean  "power_signed_up",     limit: 1,                         default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "stats_unsent",        limit: 1,                         default: true
    t.boolean  "stale_shifts_unsent", limit: 1,                         default: true
    t.boolean  "missed",              limit: 1,                         default: false
    t.boolean  "late",                limit: 1,                         default: false
    t.boolean  "left_early",          limit: 1,                         default: false
    t.boolean  "parsed",              limit: 1,                         default: false
    t.decimal  "updates_hour",                  precision: 5, scale: 2, default: 0.0
  end

  add_index "shifts", ["user_id"], name: "index_shifts_on_user_id", using: :btree

  create_table "shifts_tasks", id: false, force: :cascade do |t|
    t.integer  "task_id",    limit: 4
    t.integer  "shift_id",   limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "missed",     limit: 1
  end

  create_table "sub_requests", force: :cascade do |t|
    t.datetime "start"
    t.datetime "end"
    t.datetime "mandatory_start"
    t.datetime "mandatory_end"
    t.text     "reason",          limit: 65535
    t.integer  "shift_id",        limit: 4
    t.integer  "user_id",         limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sub_requests_users", id: false, force: :cascade do |t|
    t.integer "sub_request_id", limit: 4
    t.integer "user_id",        limit: 4
  end

  create_table "tasks", force: :cascade do |t|
    t.integer  "location_id",        limit: 4
    t.string   "name",               limit: 255
    t.string   "kind",               limit: 255
    t.datetime "start"
    t.datetime "end"
    t.boolean  "interval_completed", limit: 1,   default: false
    t.time     "time_of_day"
    t.string   "day_in_week",        limit: 255
    t.boolean  "active",             limit: 1,   default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "expired",            limit: 1,   default: false
    t.string   "description",        limit: 255
    t.string   "link",               limit: 255
  end

  create_table "template_time_slots", force: :cascade do |t|
    t.integer  "location_id", limit: 4
    t.integer  "template_id", limit: 4
    t.integer  "day",         limit: 4
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "templates", force: :cascade do |t|
    t.string   "name",                 limit: 255
    t.integer  "department_id",        limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public",               limit: 1
    t.integer  "max_total_hours",      limit: 4
    t.integer  "min_total_hours",      limit: 4
    t.integer  "max_continuous_hours", limit: 4
    t.integer  "min_continuous_hours", limit: 4
    t.integer  "max_number_of_shifts", limit: 4
    t.integer  "min_number_of_shifts", limit: 4
    t.integer  "max_hours_per_day",    limit: 4
  end

  create_table "time_slots", force: :cascade do |t|
    t.integer  "location_id",        limit: 4
    t.integer  "calendar_id",        limit: 4
    t.integer  "repeating_event_id", limit: 4
    t.datetime "start"
    t.datetime "end"
    t.boolean  "active",             limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_configs", force: :cascade do |t|
    t.integer  "user_id",                limit: 4
    t.integer  "default_dept",           limit: 4
    t.string   "view_loc_groups",        limit: 255
    t.string   "view_week",              limit: 255
    t.string   "watched_data_objects",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "taken_sub_email",        limit: 1,   default: true
    t.boolean  "send_due_payform_email", limit: 1,   default: true
  end

  create_table "user_profile_entries", force: :cascade do |t|
    t.integer  "user_profile_id",       limit: 4
    t.integer  "user_profile_field_id", limit: 4
    t.string   "content",               limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_profile_fields", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.string   "display_type",  limit: 255
    t.string   "values",        limit: 255
    t.boolean  "public",        limit: 1
    t.boolean  "user_editable", limit: 1
    t.integer  "department_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "index_display", limit: 1,   default: true
  end

  create_table "user_profiles", force: :cascade do |t|
    t.integer  "user_id",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 4
    t.datetime "photo_updated_at"
    t.integer  "crop_x",             limit: 4
    t.integer  "crop_y",             limit: 4
    t.integer  "crop_h",             limit: 4
    t.integer  "crop_w",             limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "login",                 limit: 255
    t.string   "first_name",            limit: 255
    t.string   "last_name",             limit: 255
    t.string   "nick_name",             limit: 255
    t.string   "employee_id",           limit: 255
    t.string   "email",                 limit: 255
    t.string   "crypted_password",      limit: 255
    t.string   "password_salt",         limit: 255
    t.string   "persistence_token",     limit: 255
    t.string   "auth_type",             limit: 255
    t.string   "perishable_token",      limit: 255, default: "",   null: false
    t.integer  "default_department_id", limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "superuser",             limit: 1
    t.boolean  "supermode",             limit: 1,   default: true
    t.string   "calendar_feed_hash",    limit: 255
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",  limit: 255,   null: false
    t.integer  "item_id",    limit: 4,     null: false
    t.string   "event",      limit: 255,   null: false
    t.string   "whodunnit",  limit: 255
    t.text     "object",     limit: 65535
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

end
